import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { HttpErrorResponse } from '@angular/common/http'
@Component({
  selector: 'app-showemployee',
  templateUrl: './showemployee.component.html',
  styleUrl: './showemployee.component.css'
})
export class showemployeeComponent implements OnInit {
  employees: any[] = [];
  emailId: any;
  emp: any;

  constructor(private service: EmpService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.loadEmployees();
  }

  loadEmployees() {
    this.service.getAllEmployees().subscribe((data: any) => {
      this.emp = data;
      this.employees = data;
      console.log(data);
    });
  }

  deleteEmployeeById(employee: any) {
    const i = this.employees.findIndex((element: any) => element.id == employee.id);
    if (i !== -1) {
      const deletedEmployeeId = this.employees[i].empId;
      this.service.deleteEmployeeById(deletedEmployeeId).subscribe(
        () => {
          this.employees.splice(i, 1);
          this.loadEmployees;
        },
        (error: any) => {
          console.error('Error deleting employee:', error);
          // Log the error details
          if (error instanceof HttpErrorResponse) {
            console.error('Status:', error.status);
            console.error('Response body:', error.error);
          }
        }
      );
    }
  }
  

  submit() {
    console.log(this.employees);
  }

}

