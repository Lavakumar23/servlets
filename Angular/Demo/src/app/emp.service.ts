import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EmpService {
  isUserLoggedIn: any;
  loginStatus: any;
  cartItems: any;

  constructor(private http:HttpClient) {
this.cartItems=[];

  this.loginStatus = false;
    this.isUserLoggedIn = new Subject();
  }
  addToCart(product:any){
    this.cartItems.push(product);
  }
  getCartItems(): any {
    return this.cartItems;
  }

  setCartItems() {
    this.cartItems.splice();
  }
  
  //Dependency Injection for HttpClient


  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }
  
  getAllEmployees():any{
    return this.http.get('http://localhost:8085/getEmployees')
  }
  getEmpById(empId:any):any{
    return this.http.get('http://localhost:8085/getEmployeeById/'+empId)
  }
  getAllDepartments(): any {
    return this.http.get('http://localhost:8085/getDepartments');
  }
  regsiterEmployee(employee: any): any {
    return this.http.post('http://localhost:8085/addEmployee', employee);
  }
  deleteEmployeeById(empId: any):any{
    return this.http.delete('http://localhost:8085/deleteEmployeeById/'+empId);
  }
  //Login
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
  }

  getIsUserLogged(): any {
    return this.isUserLoggedIn;
  }

  //Logout
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
  }

}
