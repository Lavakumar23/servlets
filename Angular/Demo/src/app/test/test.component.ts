import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit{
 
   id:number;
   name:string;
   avg:number;
   address:any;
   hobbies:any;
  constructor(){
   // alert("consturctor invoked")
    this.id=1;
    this.name='lavakumar';
    this.avg=50.23;
    this.address={
      streetNo:102,
      city:"hyd",
      state:"telangana"
    };
    this.hobbies=['sleeping','Eating','swimming','singing','dancing'];
  }
  ngOnInit() {
     // alert("ngoninit invoked")
  }
}
