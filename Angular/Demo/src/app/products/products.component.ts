import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent implements OnInit{
  products:any;
  emailId:any;
  selectedProduct:any;
  cartProducts: any;
  constructor(private service:EmpService){
    this.emailId=localStorage.getItem('emailId');
    this.cartProducts=[];
  
 this.products = [
    {
      name: 'BENZ',
      description: 'Super fast car with 380kmph',
      price: 2500000,
      imageUrl: './assets/Benz.jpg'
    },
    {
      name: 'RollsRoyce',
      description: 'worlds luxuorys car',
      price: 5000000,
      imageUrl: './assets/RollsRoyce.jpg'
    },
    {
      name: 'Nexon',
      description: ' worlds safest car',
      price: 750,
      imageUrl: './assets/Nexon.jpg'
    },
    {
      name: 'Bentley',
      description: 'worlds fastest car with 430kmph',
      price: 70000000,
      imageUrl: './assets/Bentley.jpg'
    },
    {
      name: 'THAR',
      description: 'offroading car ',
      price: 400000,
      imageUrl: './assets/THAR.jpg'
    },
    {
      name: 'BMW',
      description: 'car with a speed of 300kmph',
      price: 6000000,
      imageUrl: './assets/BMW.jpg'
    }
   
  ];
}
  ngOnInit()
  {

  }
addToCart(product:any)
{
  this.service.addToCart(product);
}
}

