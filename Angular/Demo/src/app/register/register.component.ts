import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  
  empName: any;
  salary: any;
  gender: any;
  doj: any;
  country: any;
  phoneNumber: any;
  emailId: any;
  password: any;
  countries:any;
  departments:any;
  emp: any;
  constructor(private router: Router, private toastr: ToastrService,private service:EmpService) {
  this.emp={ empName:'',
  salary:'',
  gender:'',
  doj:'',
  country:'',
  emailId:'',
  password:'',
  department: {
    deptId:''}
  }
}

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => {this.countries = data;});
    this.service.getAllDepartments().subscribe((data: any) => {this.departments = data;});
    
  }

  submit() {
    console.log("EmpName: " + this.empName);
    console.log("Salary: " + this.salary);
    console.log("Gender: " + this.gender);
    console.log("DateOfJoin: " + this.doj);
    console.log("Country: " + this.country);
    console.log("PhoneNumber: " + this.phoneNumber);
    console.log("Email-Id: " + this.emailId);
    console.log("Password: " + this.password);
  }

  registerSubmit(regForm :any) {
    console.log(regForm);
    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.department.deptId = regForm.department;

    console.log(this.emp);

    this.service.regsiterEmployee(this.emp).subscribe((data: any) => {console.log(data);});

    this.router.navigate(['login']);
    
    if (!this.validateForm()) {
      return;
    }
    const newEmployee = {
      empId: this.generateEmpId(),
      empName: this.empName,
      salary: this.salary,
      gender: this.gender,
      doj: this.doj,
      country: this.country,
      phoneNumber: this.phoneNumber,
      emailId: this.emailId,
      password: this.password
    };

    console.log(newEmployee);

    this.toastr.success('Registration Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });

    this.router.navigate(['login']);
  }

  private validateForm(): boolean {

    if (!this.empName || !this.salary || !this.emailId || !this.password) {
      this.toastr.error('Please fill in all required fields.', 'Error', {
        closeButton: true,
        progressBar: true,
        positionClass: 'toast-top-right',
        tapToDismiss: false,
        timeOut: 3000, // 3 seconds
      });
      return false;
    }
    return true;
  }

  private generateEmpId(): number {
    return Math.floor(Math.random() * 1000) + 1;
  }
}






