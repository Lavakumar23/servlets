import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showemp-by-id',
  templateUrl: './showemp-by-id.component.html',
  styleUrl: './showemp-by-id.component.css'
})
export class ShowempByIdComponent implements OnInit{
  
  empId: any;
  emp: any;

  constructor(private service:EmpService) {
    
  }

  ngOnInit() {
  }

  getEmpById(emp: any) {
    this.service.getEmpById(emp.empId).subscribe(((data:any)=>{this.emp=data}));
   
    
    
  }
}
