import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { authGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { showemployeeComponent } from './showemployee/showemployee.component';
import { ShowempByIdComponent } from './showemp-by-id/showemp-by-id.component';
import { CartComponent } from './cart/cart.component';

const routes: Routes = [
  {path:'',            component:LoginComponent},
  {path:'login',       component:LoginComponent},
  {path:'register',    component:RegisterComponent},
  {path:'showemps',    canActivate:[authGuard], component:showemployeeComponent},
  {path:'showempbyid', canActivate:[authGuard], component:ShowempByIdComponent},
  {path:'products',    canActivate:[authGuard], component:ProductsComponent},
  {path:'logout',      canActivate:[authGuard], component:LogoutComponent},
  {path:'cart',component:CartComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
