package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StudentHomePage
 */
@WebServlet("/StudentHomePage")
public class StudentHomePage extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter printWriter = response.getWriter();
		
		String emailId = request.getParameter("emailId");
		
		printWriter.println("<body bgcolor='lightblue' text='green'>");
		printWriter.println("<h3 style='color:red;'>Welcome " + emailId + "!</h3>");
		
		printWriter.println("<form align='right'>");
		printWriter.println("<a href='LogoutServlet'>Logout</a>");
		printWriter.println("</form>");
		
		printWriter.println("<body bgcolor='lightyellow' text='green'>");
		printWriter.println("<center><h1>Welcome to TeacherHomePage</h1>");
		printWriter.println("</center>");
		printWriter.println("</body>");
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
